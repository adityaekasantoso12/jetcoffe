package com.aditya.jetcoffee.model

import androidx.compose.ui.graphics.vector.ImageVector

data class BottomBarItem(val title: String, val icon: ImageVector)

