package com.aditya.jetcoffee.model

import com.aditya.jetcoffee.R

data class Menu(
    val image: Int, //Gambar menu
    val title: String, //Nama menu
    val price: String, //Harga menu
)

val dummyMenu = listOf( //Sumber data untuk menampilkan menu-menu
    Menu(R.drawable.menu1, "Tiramisu Coffee Milk", "Rp 28.000"),
    Menu(R.drawable.menu2, "Pumpkin Spice Latte", "Rp 18.000"),
    Menu(R.drawable.menu3, "Light Cappuccino", "Rp 20.000"),
    Menu(R.drawable.menu4, "Choco Creamy Latte", "Rp 16.000"),
)

val dummyBestSellerMenu = dummyMenu.shuffled() //Mengacak urutan elemen-elemennya