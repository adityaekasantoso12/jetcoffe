package com.aditya.jetcoffee.ui.components

import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@Composable
fun HomeSection( //Membuat sebuah section di dalam halaman beranda
    title: String, //Judul atau nama dari section
    modifier: Modifier = Modifier, //Objek Modifier yang digunakan untuk memodifikasi tampilan komponen secara keseluruhan
    content: @Composable () -> Unit //Lambda yang berisi konten dari section yang akan ditampilkan
) {
    Column(modifier) {
        SectionText(title, modifier)
        content()
    }
}