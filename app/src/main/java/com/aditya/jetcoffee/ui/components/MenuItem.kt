package com.aditya.jetcoffee.ui.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.aditya.jetcoffee.R
import com.aditya.jetcoffee.model.Menu

@Composable
fun MenuItem(
    menu: Menu,
    modifier: Modifier = Modifier,
) {
    ElevatedCard(
        modifier = modifier.width(140.dp), //Lebar card
        shape = RoundedCornerShape(8.dp), //Card supaya tumpul
        colors = CardDefaults.cardColors( //Warna cards
            containerColor = MaterialTheme.colorScheme.background,
        ),
    ) {
        Column {
            Image(
                painter = painterResource(menu.image), ////Mengambil gambar dari data class di Menu.kt
                contentDescription = null, //Deskripsi gambar
                contentScale = ContentScale.Crop,//Gambar full sampai kecrop
                modifier = Modifier
                    .fillMaxWidth() //Mengisi seluruh lebar yang tersedia
                    .height(120.dp) //Tinggi gambar
                    .clip(RoundedCornerShape(8.dp)) //Menentukan sudut gambar
            )
            Column(modifier = Modifier.padding(8.dp)) {
                Text(
                    text = menu.title, //Menampilkan nama menu
                    maxLines = 2, //menentukan jumlah baris maksimal dari sebuah teks
                    overflow = TextOverflow.Ellipsis,
                    style = MaterialTheme.typography.titleMedium.copy(
                        fontWeight = FontWeight.ExtraBold //Style nama menu exstra bold
                    ),
                )
                Text(
                    text = menu.price, //Menampilkan harga menu
                    style = MaterialTheme.typography.titleSmall, //Style harga menu samll
                )
            }
        }
    }
}

@Composable
@Preview(showBackground = true)
fun MenuItemPreview() {
    MaterialTheme {
        MenuItem( //Menampilkan preview dari R.drawable
            menu = Menu(R.drawable.menu2, "Hot Pumpkin Spice Latte Premium", "Rp 18.000"),
            modifier = Modifier.padding(8.dp)
        )
    }
}