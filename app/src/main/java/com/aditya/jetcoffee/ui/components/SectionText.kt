package com.aditya.jetcoffee.ui.components

import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp

@Composable
fun SectionText( //Membuat sebuah section di dalam halaman beranda
    title: String, ////Judul atau nama dari section
    modifier: Modifier = Modifier ////Objek Modifier yang digunakan untuk memodifikasi tampilan komponen secara keseluruhan
) {
    Text(
        text = title, //Parameter text digunakan untuk menentukan teks yang akan ditampilkan oleh komponen Text
        style = MaterialTheme.typography.headlineSmall.copy( //menentukan gaya huruf dari teks yang ditampilkan
            fontWeight = FontWeight.ExtraBold
        ),
        modifier = modifier
            .padding(horizontal = 16.dp, vertical = 8.dp) //Jarak/space
    )
}