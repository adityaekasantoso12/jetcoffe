package com.aditya.jetcoffee.ui.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.paddingFromBaseline
import androidx.compose.foundation.layout.requiredSize
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.aditya.jetcoffee.R
import com.aditya.jetcoffee.model.Category
import com.aditya.jetcoffee.ui.theme.JetCoffeeTheme

@Composable
fun CategoryItem(
    category: Category, //Parameter category adalah objek Category
    modifier: Modifier = Modifier, //Parameter modifier adalah objek Modifier
) {
    Column( //Mengatur tampilan elemen secara vertikal
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally //Diatur ditengah secara horizontal

    ) {
        Image( //Gambar
            painter = painterResource(category.imageCategory), //Mengambil gambar dari Category.kt
            contentDescription = null, //Deskripsi gambar
            modifier = Modifier
                .requiredSize(60.dp) //Menentukan ukuran dengan angka yang pasti
                .clip(CircleShape) // Memangkas gambar menjadi bentuk lingkaran

        )
        Text( //Text
            text = stringResource(category.textCategory), //Mengambil text dari Category.kt
            fontSize = 10.sp, //Ukuran Font
            modifier = Modifier
                .paddingFromBaseline(top = 16.dp, bottom = 8.dp) //Memberi jarak atas dan bawah text
        )
    }
}

@Composable
@Preview(showBackground = true)
fun CategoryItemPreview() {
    JetCoffeeTheme {
        CategoryItem(
            category = Category( //Menampilkan preview dari R.drawable
                R.drawable.icon_category_cappuccino,
                R.string.category_cappuccino,
            ),
            modifier = Modifier.padding(horizontal = 8.dp)
        )
    }
}